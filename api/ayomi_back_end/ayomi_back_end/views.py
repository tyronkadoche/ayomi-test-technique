from rest_framework import viewsets

from api.ayomi_back_end.ayomi.models import User
from api.ayomi_back_end.ayomi_back_end.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer