from rest_framework import serializers
from api.ayomi_back_end.ayomi.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('userName', 'password')