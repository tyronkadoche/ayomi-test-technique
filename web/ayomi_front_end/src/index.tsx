import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import MainPage from "./pages/MainPage";

const hist = createBrowserHistory();

ReactDOM.render(
    <Router history={hist}>
        <Switch>
            <Route path="/auth/login" component={LoginPage} />
            <Route path="/main-page" component={MainPage} />
            <Redirect from="/" to="/auth/login" />
        </Switch>
    </Router>,
    document.getElementById("root")
);
