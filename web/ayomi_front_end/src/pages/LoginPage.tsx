import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Grid, FormControl, Input, InputLabel, IconButton, InputAdornment, Button, makeStyles } from "@material-ui/core";
import Email from "@material-ui/icons/Email";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import axios from "axios";
import baseUrl from "../api/";
import loginPageStyles from "../styles/pages/loginPageStyles";

const useStyles = makeStyles(loginPageStyles);

export default function LoginPage(): JSX.Element {
    const [userName, setUserName] = useState("")
    const [userNameError, setUserNameError] = useState("")
    const [password, setPassword] = useState("")
    const [passwordError, setPasswordError] = useState("")
    const [showPassword, setShowPassword] = useState(false);

    const classes = useStyles();

    const history = useHistory();

    function logIn() {
        const body = {
            "userName": userName,
            "password": password
        };

        axios({
            url: `http://127.0.0.1:8080/login`,
            method: "POST",
            data: body,
        })
            .then(response => {
                localStorage.setItem("token", response.data.data);
                history.replace("/main-page");
            })
            .catch((error: any) => {
                if (error.response.data.message.includes("Email")) {
                    setUserNameError(error.response.data.message);
                    setPasswordError("");
                } else if (error.response.data.message.includes("Password")) {
                    setPasswordError(error.response.data.message);
                    setUserNameError("");
                }
            });
    }

    return (
      <Grid container className={classes.authContainer}>
          <Grid item justify="center" xs={12}>
              <FormControl className={classes.authInput}>
                  <InputLabel htmlFor="input-with-icon-adornment">Email</InputLabel>
                  <Input
                      id="standard-adornment-password"
                      type="text"
                      value={userName}
                      onChange={e => setUserName(e.target.value)}
                      endAdornment={(
                          <InputAdornment position="end">
                              <IconButton aria-label="toggle password visibility">
                                  <Email />
                              </IconButton>
                          </InputAdornment>
                      )}
                  />
              </FormControl>
          </Grid>
          <Grid item justify="center" xs={12}>
              <FormControl className={classes.authInput}>
                  <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                  <Input
                      id="standard-adornment-password"
                      type={showPassword ? "text" : "password"}
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      endAdornment={(
                          <InputAdornment position="end">
                              <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={() => setShowPassword(!showPassword)}
                              >
                                  {showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                          </InputAdornment>
                      )}
                  />
              </FormControl>
          </Grid>
          <Grid item justify="center" xs={12}>
              <Button color="primary" variant="contained" disabled={!(userName.length !== 0 && password.length !== 0)} onClick={() => logIn()}>
                  Login
              </Button>
          </Grid>
      </Grid>
    );
}