import React from "react";
import { useHistory } from "react-router-dom";
import { Grid, Button } from "@material-ui/core";

export default function MainPage(): JSX.Element {

    const history = useHistory();

    function logout() {
        localStorage.clear();
        history.replace("/auth/login");
    }

    return (
        <Grid container>
            <Grid item justify="center" xs={12}>
                <Button color="primary" variant="contained" onClick={() => logout()}>
                    Logout
                </Button>
            </Grid>
        </Grid>
    );
}