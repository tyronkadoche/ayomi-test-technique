/* eslint-disable @typescript-eslint/prefer-as-const */
import { Theme } from "@material-ui/core";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const loginPageStyles = (theme: Theme) => ({
    authContainer: {
        padding: "20rem",
        textAlignLast: "center" as "center",
    },
    authInput: {
        marginBottom: "2rem",
    },
    authButton: {

    }

});

export default loginPageStyles;
